# Simple Makefile for ACS 6305ENG Assignment 3&4 #

CC=gcc
RM=rm -rf
SERVER=chat_server
CLIENT=chat_client

all:	$(SERVER) $(CLIENT)
	
$(SERVER):	chatserver.c chathelper.c
	$(CC) -g chatserver.c chathelper.c -o $(SERVER)

$(CLIENT): chatclient.c chathelper.c
	$(CC) -g chatclient.c chathelper.c -o $(CLIENT)

clean:
	$(RM) $(SERVER) $(CLIENT)
