#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>

#include "chathelper.h"

typedef struct {
	int socketNum;
	char username[MAX_USER_LEN];
	char IPaddress[INET6_ADDRSTRLEN];
	int status;
} User;

// get sockaddr, IPv4 or IPv6:
void *get_in_addr(struct sockaddr *sa)
{
 	if (sa->sa_family == AF_INET) 
 	{
 		return &(((struct sockaddr_in*)sa)->sin_addr);
 	}
 	return &(((struct sockaddr_in6*)sa)->sin6_addr);
}


int ReadMessage(char *bufferToBeRead, int socketNum, char *bufferPtr, int *currBufferSize)
{
	char *nullPtr = {'\0'};
	int messageRead = 0;
	int bytes = recv(socketNum, bufferToBeRead, MAX_BUF_SIZE - *currBufferSize, 0);
	*currBufferSize += bytes;

	if(bytes <= 0)
	{
		// got error or connection closed by client
 		if (bytes == 0) 
 		{
 			// connection closed
 			memset(bufferToBeRead, 0 ,sizeof(bufferToBeRead));
 			fprintf(stderr, "Reply from Server: socket %d hung up\n", socketNum);
 			return SOCK_HUNGUP;
 		} else {
 			fprintf(stderr, "Error receiving data from client\n");
 			return SOCK_TERM;
 		}
	}

	// is message all read ?
	int pos;
	for(pos = 0; pos < *currBufferSize; pos++)
	{
		if( *(bufferToBeRead + pos) == NEWLINE)
		{
			break;
		}
	}

	if(pos > 0)
	{
		memcpy (bufferToBeRead + pos, &nullPtr, 1);

		messageRead = 1; // We hav received the entire messag einput from user

		*currBufferSize -= pos+1;
	}

	if(messageRead)
    {
    	if(strcmp(bufferToBeRead, "quit") == 0)
    		return 3;
    }
	bufferPtr = bufferToBeRead + *currBufferSize;

	return messageRead;
}

void LogActivity(User *user, int MessageType)
{
    FILE *logFile = fopen("log_chatserver.txt", "a");

    if (logFile == NULL)
    {
         perror("Error opening file!\n");
         exit(1);
    }

    switch(MessageType)
    {
    	case MSG_DISCONN_HUNGUP:
    		fprintf(logFile, "User %s on socket %d has Hung Up\n", user->username, user->socketNum);
    	break;

    	case MSG_DISCONN_TERM:
    		fprintf(logFile, "User %s on socket %d has Disconnected\n", user->username, user->socketNum);
    	break;

    	case MSG_CLIENT_CONN:
    		fprintf(logFile, "User %s on socket %d has Connected with IP address %s\n", user->username, user->socketNum, user->IPaddress);
    	break;

    	case MSG_INV_USERNAME:
    		fprintf(logFile, "User %s on socket %d has invalid username with IP address %s\n", user->username, user->socketNum, user->IPaddress);
    	break;

    	default:
    		fprintf(logFile, "System Error : Unknown Log Message Type {%d} \n", MessageType);
    	break;
    }

    fclose(logFile);
}

int IsValidUsername(char *newName, User *users)
{
	
	int pos;
	for(pos = 0; pos < BACKLOG; pos++)
	{
		if(strcmp(users[pos].username, newName) == 0)
		{
			return FALSE;
		}
	}
	
	return TRUE;
}

int main (void) 
{
	int listener, fdNew, fdMax, bytes, currBufferSize, numConnUsers, nameErr;
	struct addrinfo hints;
	struct addrinfo *servinfo, *p;
	struct sockaddr_storage clientAddr; 
	socklen_t addrlen;
	fd_set master, temp_fds;  // hold existing and a copy of existing connections since select modifies the temp_fds
	char buffer [MAX_BUF_SIZE];
	char clientIP [INET6_ADDRSTRLEN];
	char *bufferPtr;
	User users[BACKLOG];
	User *user;

	// reset entries
	FD_ZERO(&master);
	FD_ZERO(&temp_fds);

	memset(&hints, 0, sizeof(hints)); // ensure hint has memory allocated
	memset(&users, 0, BACKLOG * sizeof(users));

	// Setup hints
	hints.ai_family = AF_INET;		// set hints about port
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE;

	currBufferSize = 0;
	numConnUsers = 0;
	nameErr = 0;

	// get address info for PORT
	GetAddressInfo(NULL, &hints, &servinfo);

	// get socket descriptor
	GetSocketDescriptor(p, servinfo, &listener);

	// binding failed
	CheckBinding(p);

	// we have finished with the addrinfo struct
	freeaddrinfo(servinfo);

	// listen
	ListenWrapper(&listener);

	// add listener to master set
	FD_SET(listener, &master);

	// assign fdmax var to listener 
	fdMax = listener;

	bufferPtr = buffer;

	printf("Waiting for connections...\n");

	while(TRUE)
	{
		// copy master to temp
		temp_fds = master;

		// start monitoring
		SelectWrapper(fdMax, &temp_fds, NULL);

		// loop through existing connections looking for data
		int i;
		for(i = 0; i <= fdMax; i++)
		{
			// we have a connection ready for reading
			if(FD_ISSET(i, &temp_fds))
			{
				// accept incoming connection
				if(i == listener)
				{
					addrlen = sizeof(clientAddr); // get client size

					// accept connection
					if( (fdNew = accept(listener, (struct sockaddr *)&clientAddr, &addrlen)) < 0)
					{
						fprintf(stderr, "Error accepting incoming connection\n");
						fprintf(stderr, "Connection Refused from %s on socket %d\n", inet_ntop(clientAddr.ss_family, get_in_addr((struct sockaddr*)&clientAddr), clientIP, INET6_ADDRSTRLEN), fdNew);
					} 
					else 
					{
						// add to master set
						FD_SET(fdNew, &master);

						// create a new user & assign socket number 
						user = &users[fdMax+1];
						user->socketNum = fdNew;

						// increase fdMax if new connection
						if(fdNew > fdMax)
						{
							fdMax = fdNew;
						}

						printf("Reply from Server: new connection from %s on socket %d\n",
 							inet_ntop(clientAddr.ss_family, get_in_addr((struct sockaddr*)&clientAddr), clientIP, INET6_ADDRSTRLEN), fdNew);

						memcpy(user->IPaddress, clientIP, sizeof(clientIP));

						numConnUsers += 1;
					}
				} 
				else 
				{
 					// attempt to get data from the client
					int readStatus;
					if((readStatus = ReadMessage(buffer, i, bufferPtr, &currBufferSize)) == MSG_READ) 
 					{
 						currBufferSize = 0;
				    }

				    if(readStatus == SOCK_QUIT)
				    {
				    	user[i].status = LOGGED_OUT;
				    	
				    }

					if(readStatus == SOCK_HUNGUP || readStatus == SOCK_TERM)
					{
 						// close connection
 						close(i);
 						// remove connection from master set
 						FD_CLR(i, &master);
 						// remove user from list of users & log disconnection
 						LogActivity(&users[i], MSG_DISCONN_HUNGUP);

				    	memset(&users[i], 0, sizeof(users[i]));
				    	numConnUsers -= 1;
					}

					if(users[i].status != LOGGED_IN) // New user logged in ?
					{
					    int buffSize = 80;
					    time_t currTime;
					    time (&currTime);
					    char timeBuffer[buffSize];
					    struct tm *timeinfo = localtime (&currTime);
					    strftime(timeBuffer, buffSize,"Welcome to Chat 101 %c", timeinfo);

					    int *len = &buffSize;

					    if(SendAll(&i, timeBuffer , len) == SENDERR)
					    {
					    	perror("Error server welcome new client connection : ");
					    	break;
					    }
					    else
					    {
					    	char *name = buffer;

					    	if(IsValidUsername(name, &users[0]) == TRUE)
					    	{
					    		users[i].status = LOGGED_IN;
					    		size_t users_size = sizeof (users[i].username);
					    		strncpy(users[i].username, buffer, users_size);
					    		printf("User %s on port %d logged in\n",users[i].username, i);
					    		LogActivity(&users[i], MSG_CLIENT_CONN);

					    	}
					    	else
					    	{
					    		int len = MAX_USER_LEN;
					    		if (SendAll(&i, "ERROR", &len) == -1) 
 								{
								 	perror("Error with sending : ");
 								}
 								nameErr = 1;
					    	}
					    	
					    }
					}
					else  /** Server broadcasts message to all sockets apart from listenre  and the current connected socket. **/
					{ 	
						char formattedMsg[MAX_BUF_SIZE];
 						int len = MAX_BUF_SIZE;
 						memset(&formattedMsg, 0, sizeof(formattedMsg));	  

					    int j;
 						for(j = 0; j <= fdMax; j++) 
 						{
 							// send to just us
 							if (FD_ISSET(j, &master)) 
 							{
 								
 								if( j == i && j != listener)
 								{

 									if(users[i].status == LOGGED_OUT || nameErr == 1)
 									{
 										// remove user from list of users & log disconnection
 										LogActivity(&users[i], MSG_DISCONN_HUNGUP);

				    					printf("%s on socket %d has quit the chat room\n", users[i].username, users[j].socketNum);

				    					memset(&users[i], 0, sizeof(users[i]));

				    					// close connection
 										close(i);
 										// remove connection from master set
 										FD_CLR(i, &master);

 										nameErr = 0; // reset name error

 									}
 									
 								}
 								else if (j != listener && j != i && i != 0) // send to all
 								{
 									strcat(formattedMsg, users[i].username);
 									strcat(formattedMsg, " : ");
 									strcat(formattedMsg, buffer);

 									if (SendAll(&j, formattedMsg, &len) == -1) 
 									{
								 		perror("Error with sending : ");
 									}		
 								}
 								
 							}
 						}
					}
					memset(buffer, 0 ,sizeof(buffer));

				} // END handle data from client
			}
		}
	}
}