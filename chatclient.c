#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <pthread.h>
#include "chathelper.h"

#define NOT_CONNECTED 0
#define CONNECTED 1
#define BUFFER_SIZE 4096

void CheckArgs(int numArgs)
{
	if(numArgs != 3)
	{
		fprintf(stderr, "Incorrect number of args detected\nPlease use the format <prog name> <serverip> <username> \n");
	}
}

int GetUserInput(char *messagePtr, char *msgPosPtr, int *currBufferSize)
{
	char *nullPtr = {'\0'};
	int messageRead = 0;
	int bytes = read(STDIN_FILENO, msgPosPtr, BUFFER_SIZE - *currBufferSize);
	*currBufferSize += bytes;

	/** HAve we got all the user input yet? **/
	int pos;
	for(pos = 0; pos < *currBufferSize; pos++)
	{
		if( *(messagePtr + pos) == NEWLINE)
		{
			break;
		}
	}

	if(pos > 0)
	{
		memcpy (messagePtr + pos, &nullPtr, 1);

		messageRead = 1; // We hav received the entire messag einput from user

		*currBufferSize -= pos+1;
	}

	msgPosPtr = messagePtr + *currBufferSize;

	return bytes == -1 ? -1 : messageRead;
}

int main(int argc, char *argv[])
{
	struct sockaddr_in serv_addr;
	struct addrinfo hints;
	struct addrinfo *clientinfo, *pClient;
	int svrSocket= 0;
	char *host;
	char *username, *msgPosPtr;
	char serverIP[INET6_ADDRSTRLEN];
	struct hostent *server;
	int status = NOT_CONNECTED;
	int currBufferSize = 0;

	CheckArgs(argc);

	username = argv[2];

	if(!strcmp(argv[1], "localhost"))
	{
		host = argv[1];
	} 
	else 
	{
		host = NULL;
	}

	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;

	GetAddressInfo(host, &hints, &clientinfo);
	
	for(pClient = clientinfo; pClient != NULL; pClient = pClient->ai_next)					 
	{
		svrSocket = socket(AF_INET, SOCK_STREAM, 0);

		if(svrSocket < 0) { perror("Failed to create socket\n"); continue; }

		server = gethostbyname(argv[1]);
    	if (server == NULL) 
    	{
       		 fprintf(stderr,"ERROR, no such host\n");
       		 exit(0);
   		}
		// overcome reconnecting to a socket that still may not have closed completely
		int isClosed;
		if(setsockopt(svrSocket, SOL_SOCKET, SO_REUSEADDR, &isClosed, sizeof(int)) < 0)
		{
			perror("Problems setting reuse address in setsockopt\n");
			exit(errno);
		}

		memset(&serv_addr, '0', sizeof(serv_addr));
		serv_addr.sin_family = AF_INET;
		serv_addr.sin_addr.s_addr= inet_addr(argv[1]);
		serv_addr.sin_port = htons(5006);

		// bind port
		int didConnect;
		if( (didConnect = connect(svrSocket,(struct sockaddr *)&serv_addr , sizeof(serv_addr)) == -1 ))
		{
			close(svrSocket); 
			perror("Client connection failure");
			exit(1);
		} 

		break;
	}

	char message[BUFFER_SIZE];
	char server_reply[BUFFER_SIZE];

	memset(&message, 0, sizeof(message));

	struct timeval tv; tv.tv_sec = 2; tv.tv_usec = 0;  // CHANGE NAME OF TV

	msgPosPtr = message;

	if(status == NOT_CONNECTED) 
	{
		/** Send username in first request to server **/
        if( send(svrSocket , username , strlen(username) , 0) < 0)
        {
          	  perror("Initial Send failed :");
           	  exit(1);
        }
        status = CONNECTED;

	}

	//keep communicating with server
 
	while(TRUE) 
	{
		fd_set readFds, sendFds;

		/** Add to File Descriptor Set **/
		FD_SET(svrSocket, &readFds);
		FD_SET(STDIN_FILENO, &readFds);

		SelectWrapper(svrSocket+1, &readFds, &tv);

		
		if(FD_ISSET(STDIN_FILENO, &readFds)) 
		{
			
			if(status == CONNECTED)
			{
				if(GetUserInput(message, msgPosPtr, &currBufferSize) == MSG_COMPLETE)
				{	
					int len = BUFFER_SIZE;
					int *lenPtr = &len;

					// read
					if( SendAll(&svrSocket , message , lenPtr) == SENDERR)
        			{
              	 		 perror("send failed :");
       	   	   	    } 
       	   	   	    else
       	   	   	    {
       	   	   	    	currBufferSize = 0;

        	    		if(strcmp(message, "quit") == 0)
 						{
 							printf("You have left the chat room\n");
 							exit(1);
 						}

 						memset(&message, 0, sizeof(message));
       	   	   	    }
				}
			}
		
		}

		if(FD_ISSET(svrSocket, &readFds))
		{
			int bytesRead = 0;
			// send
			int receiveAll = 0;
			while(receiveAll == 0)
			{
				int bytesRecv = recv(svrSocket, server_reply , BUFFER_SIZE , 0);

				switch(bytesRecv)
				{
					case 0:
						receiveAll = 1;
					break;

					case -1:
						perror("Chat Message failed :");
           	  			exit(1);
					break;

					default:
					bytesRead += bytesRecv;
					break;
				}

				/** Check to see if we have received the entire message **/
        		if(bytesRecv = BUFFER_SIZE)
        		{
        			receiveAll = 1;
        		}
			}

			if(strcmp(server_reply, "ERROR") == 0)
			{
				printf("Same name error\n");
				exit(1);
			}

        	puts(server_reply);
        	memset(&server_reply, 0, sizeof(server_reply));
		}
	}

	close(svrSocket);

	return EXIT_SUCCESS;
}

