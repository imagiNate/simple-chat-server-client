This is a simple chat server and client application.

It is coded in C and was a part of assessment for 6305ENG Advanced Computer Systems.

The main focus was on using Select function to handle multiple concurrent connections on the server side.

Any message sent from a client is relayed to all other connected clients.