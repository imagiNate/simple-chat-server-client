#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>

#define PORT "5006" 
#define BACKLOG 10  // queue size for listening
#define MAX_BUF_SIZE 4096
#define MAX_USER_LEN 64
#define NEW 1
#define LOGGED_IN 2
#define NEWLINE '\n'
#define MSG_READ 1
#define SOCK_HUNGUP -1
#define SOCK_TERM -2
#define SOCK_QUIT 3
#define SENDERR -1
#define MSG_COMPLETE 1
#define MSG_CLIENT_CONN 1
#define MSG_DISCONN_HUNGUP -1
#define MSG_DISCONN_TERM -2
#define MSG_INV_USERNAME -3
#define TRUE 1
#define FALSE 0
#define LOGGED_OUT 0

void GetAddressInfo(char *host, struct addrinfo *hints, struct addrinfo **servinfo);

void GetSocketDescriptor(struct addrinfo * p, struct addrinfo *servinfo, int *listener);

void CheckBinding(struct addrinfo *p);

void ListenWrapper(int *listener);

void SelectWrapper(int fdMax, fd_set *temp_fds, struct timeval *timeout);

int SendAll(int *socketNum, char *sendBuffer, int *bufferLen);