#include "chathelper.h"

void SelectWrapper(int fdMax, fd_set *temp_fds, struct timeval *peekTime)
{
	if(select(fdMax+1, temp_fds, NULL, NULL, peekTime) < 0)
	{
		perror("Select failed :");
		exit(0);
	}
}

void ListenWrapper(int *listener)
{
	if(listen(*listener, BACKLOG) < 0)
	{
		perror("Listening error :");
		exit(0);
	}
}

void CheckBinding(struct addrinfo *p)
{
	if(p != NULL)
	{
		perror("Failed to bind to socket : ");
		//exit(2);
	} 
}

void GetSocketDescriptor(struct addrinfo * pNetwork, struct addrinfo *servinfo, int *listener)
{
	for(pNetwork = servinfo; pNetwork != NULL; pNetwork = pNetwork->ai_next)					 
	{
		*listener = socket(pNetwork->ai_family, pNetwork->ai_socktype, pNetwork->ai_protocol);

		if(*listener < 0) { perror("Failed to create socket\n"); continue; }

		// overcome reconnecting to a socket that still may not have closed completely
		int yes;
		if(setsockopt(*listener, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) < 0)
		{
			perror("Problems setting reuse address in setsockopt\n");
			exit(errno);
		}

		// bind port
		if( bind(*listener, pNetwork->ai_addr, pNetwork->ai_addrlen) < 0 )
		{
			close(*listener); continue;
		}

		break;
	}
}

void GetAddressInfo(char *host, struct addrinfo *hints, struct addrinfo **servinfo)
{
	int status;
	if( (status = getaddrinfo(host, PORT, hints, servinfo)) != 0 )  
	{
		fprintf(stderr, "getaddrinfo error : %s\n", gai_strerror(status));
		exit(status);
	} 
}

int SendAll(int *socketNum, char *sendBuffer, int *bufferLen)
{
	int totalSent = 0;
	int remaining = *bufferLen;
	int bytes;

	while (totalSent < *bufferLen)
	{
		bytes = (send(*socketNum, sendBuffer , remaining, 0));

		if(bytes == SENDERR)
		{
			return SENDERR;
		}

		totalSent += bytes;
		remaining -= bytes;
	}

	*bufferLen = totalSent;

	return bytes == SENDERR ? SENDERR : 0;
}
